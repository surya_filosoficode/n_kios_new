<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmain extends CI_Controller{
    
	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Admin_main", "am");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != "1" and $session["is_log"] != "1"){
                redirect(base_url("back-admin/login"));
            }
        }else{
            redirect(base_url("back-admin/login"));
        }
	}

#=============================================================================#
#-------------------------------------------main_upload_file------------------#
#=============================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }
#=============================================================================#
#-------------------------------------------main_upload_file------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#

    public function index_home(){
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/admin_main/admin_home");
        $this->load->view("ad_super/footer");
    }
#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#

    public function index_admin(){
        $this->load->library('encrypt');
        $data["admin"]  = $this->am->select_admin_all(array("a.is_del"=>"0"));
        $data["dinas"]  = $this->mm->get_data_all_where("dinas", array("is_del"=>"0"));
        $data["lv"]     = $this->mm->get_data_all_where("admin_lv", array("is_del"=>"0"));

        $this->load->view("ad_super/header");
        $this->load->view("ad_super/admin_main/admin_main", $data);
        $this->load->view("ad_super/footer");
    }

    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nip',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'jbtn',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'lv',
                    'label'=>'Lv. Admin',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "nip"=>"",
                    "jabatan"=>"",
                    "id_bidang"=>"",
                    "lv"=>"",
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->val_form()){
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $nip = $this->input->post("nip");
            $jabatan = $this->input->post("jbtn");
            $id_bidang = $this->input->post("dinas");
            $lv = $this->input->post("lv");
            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            if ($pass == $repass) {
                // print_r($_POST);
                $insert = $this->db->query("select insert_admin('".$nama."','".$email."','".$nip."','".$jabatan."','".$id_bidang."','".$lv."','".hash("sha256",$pass)."','".$admin_del."','".$time_update."')");

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "email"=>strip_tags(form_error('email')),
                            "nama"=>strip_tags(form_error('nama')),
                            "nip"=>strip_tags(form_error('nip')),
                            "jabatan"=>strip_tags(form_error('jbtn')),
                            "id_bidang"=>strip_tags(form_error('dinas')),
                            "lv"=>strip_tags(form_error('lv')),
                            "pass"=>strip_tags(form_error('pass')),
                            "repass"=>strip_tags(form_error('repass'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_admin_update(){
        
        $id = $this->encrypt->decode($this->input->post("id_admin"));
        $data = $this->mm->get_data_each("admin",array("id_admin"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_admin"=>$this->encrypt->encode($data["id_admin"]),
                                                "id_dinas"=>$data["id_bidang"],
                                                "id_lv"=>$data["id_lv"],
                                                "jabatan"=>$data["jabatan"],
                                                "nama"=>$data["nama"],
                                                "nip"=>$data["nip"],
                                                "email"=>$data["email"]);
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nip',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'jbtn',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'lv',
                    'label'=>'Lv. Admin',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "nip"=>"",
                    "jabatan"=>"",
                    "id_bidang"=>"",
                    "lv"=>""
                );

        if($this->val_form_update()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));

            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $nip = $this->input->post("nip");
            $jabatan = $this->input->post("jbtn");
            $id_bidang = $this->input->post("dinas");
            $lv = $this->input->post("lv");

            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            if($this->mm->get_data_all_where("admin", array("email"=>$email, "id_admin!="=>$id_admin))){
                $msg_detail["email"] = "email sudah terdaftar, silahkan gunakan email yang belum terdaftar";
            }else{
                $set = array(
                        "nama"=>$nama,
                        "email"=>$email,
                        "nip"=>$nip,
                        "jabatan"=>$jabatan,
                        "id_bidang"=>$id_bidang,
                        "id_lv"=>$lv,
                        "admin_del"=>$admin_del,
                        "time_update"=>$time_update
                    );

                $where = array(
                            "id_admin"=>$id_admin
                        );

                $update = $this->mm->update_data("admin", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }            
        }else{
            $msg_detail["email"] = strip_tags(form_error('email'));
            $msg_detail["nama"] = strip_tags(form_error('nama'));
            $msg_detail["nip"] = strip_tags(form_error('nip'));
            $msg_detail["jabatan"] = strip_tags(form_error('jbtn'));
            $msg_detail["id_bidang"] = strip_tags(form_error('dinas'));
            $msg_detail["lv"] = strip_tags(form_error('lv'));
                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_del"=>$is_del,
                    "time_del"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function get_password(){
        $res_array = array(
                    "status"=>false,
                    "val_key"=>""
                );

        if(isset($_POST["id_admin"])){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));
            $data = $this->mm->get_data_all_where("admin", array("id_admin"=>$id_admin));

            $res_array = array(
                    "status"=>true,
                    "val_key"=>hash('sha256', $data[0]->password)
                );
        }
        print_r(json_encode($res_array));
    }

    private function validation_change_pass(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'key',
                    'label'=>'key',
                    'rules'=>'required|alpha_numeric|exact_length[64]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR"),
                        'exact_length[64]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),


                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->validation_change_pass()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));
            $key = $this->input->post("key");

            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            if($pass == $repass){
                $data_admin = $this->mm->get_data_all_where("admin", array("id_admin"=>$id_admin));
                if(hash('sha256', $data_admin[0]->password) == $key){
                    $set = array(
                            "password"=>hash("sha256",$pass),
                        );

                    $where = array(
                                "id_admin"=>$id_admin
                            );

                    $update = $this->mm->update_data("admin", $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
            }            
        }else{
            $msg_detail["pass"] = strip_tags(form_error('pass'));
            $msg_detail["repass"] = strip_tags(form_error('repass'));                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Lv_Admin--------------------#
#=============================================================================#
    public function index_admin_lv(){
        $data["lv"]     = $this->mm->get_data_all_where("admin_lv", array("is_del"=>"0"));

        $this->load->view("ad_super/admin_main/admin_main_lv", $data);
    }

    public function validation_admin_lv(){
        $config_val_input = array(
                array(
                    'field'=>'ket_lv',
                    'label'=>'Keterangan',
                    'rules'=>'required|is_unique[admin_lv.ket]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin_lv(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "ket_lv"=>""
                    );
        if($this->validation_admin_lv()){
            $ket_lv = $this->input->post("ket_lv");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $data = array(
                        "id_lv"=>"",
                        "ket"=>$ket_lv,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            if($this->mm->insert_data("admin_lv",$data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else{
            $msg_detail["ket_lv"]=strip_tags(form_error("ket_lv"));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $this->session->set_flashdata("response_lv", $res_msg);
        redirect(base_url()."ad_super/adminmain/index_admin_lv");
    }

    public function delete_admin_lv(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = null;

        $id_lv = $this->input->post("id_lv");
        $data = array(
                    "is_del"=>"1",
                    "time_del"=>date("Y-m-d h:i:s")
                );
        if($this->mm->update_data("admin_lv", $data, array("sha2(id_lv, \"256\")="=>$id_lv))){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }        

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $this->session->set_flashdata("response_lv", $res_msg);
        redirect(base_url()."ad_super/adminmain/index_admin_lv");
     }
#=============================================================================#
#-------------------------------------------Index_Lv_Admin--------------------#
#=============================================================================# 

#=============================================================================#
#-------------------------------------------Index_Dinas-----------------------#
#=============================================================================#
    public function index_dinas(){
        $data["dinas"]  = $this->mm->get_data_all_where("dinas", array("is_del"=>"0"));
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/admin_main/admin_dinas", $data);
        $this->load->view("ad_super/footer");
    }

    
    public function validate_dinas(){
        $config_val_input = array(
                array(
                    'field'=>'nm_dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "page_kelola"=>json_encode(array()), 
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );
            if($this->mm->insert_data("dinas", $data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_dinas_update(){
        $id = $this->encrypt->decode($this->input->post("id_dinas"));

        $data = $this->mm->get_data_each("dinas", array("id_dinas"=>$id));
        // print_r($data);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));

    }

    public function update_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_dinas = $this->encrypt->decode($this->input->post("id_dinas"));
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]); 

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_dinas"=>$id_dinas
                );
            if($this->mm->update_data("dinas", $data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = null;        

        if(isset($_POST["id_dinas"])){

            $id_dinas = $this->encrypt->decode($this->input->post("id_dinas"));
            $data = array(
                        "is_del"=>"1",
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_dinas"=>$id_dinas
                    );

            $update = $this->mm->update_data("dinas" ,$data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
            }
            
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Dinas-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------index_menu------------------------#
#=============================================================================#
    public function index_page_menu(){
        $data["list_menu_kategori"]     = $this->mm->get_data_all_where("home_page_kategori", array("is_delete"=>"0"));
        $data["list_menu_main"]         = $this->am->get_menu_main(array("a.is_delete"=>"0"));
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/admin_main/admin_page_main", $data);
        $this->load->view("ad_super/footer");
    }
#=============================================================================#
#-------------------------------------------index_menu------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------page_kategori---------------------#
#=============================================================================#
    public function validate_page_kategori(){
        $config_val_input = array(
                array(
                    'field'=>'ket_kategori',
                    'label'=>'Nama Kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_page_kategori(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "ket_kategori"=>""
                    );
        if($this->validate_page_kategori()){
            $ket_kategori = $this->input->post("ket_kategori");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $data = array(
                        "nama_kategori"=>$ket_kategori,
                        "is_delete"=>"0",
                        "waktu"=> $time_update,
                        "id_admin"=>$id_admin
                    );
            if($this->mm->insert_data("home_page_kategori", $data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                $msg_main = array("status"=>true, 
                                    "msg"=>$this->response_message->get_success_msg("INSERT_SUC"), 
                                    "item"=>$this->am->get_menu_kategori(array("is_delete"=> "0")));
            }
        }else {
            $msg_detail["ket_kategori"] = strip_tags(form_error('ket_kategori'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_page_kategori_update(){
        $id = $this->encrypt->decode($this->input->post("id_dinas"));

        $data = $this->mm->get_data_each("dinas", array("id_dinas"=>$id));
        // print_r($data);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));

    }

    public function update_page_kategori(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_dinas = $this->encrypt->decode($this->input->post("id_dinas"));
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]); 

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_dinas"=>$id_dinas
                );
            if($this->mm->update_data("dinas", $data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_page_kategori(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = null;        

        if(isset($_POST["id_kategori"])){

            $id_kategori = $this->encrypt->decode($this->input->post("id_kategori"));
            $data = array(
                        "is_delete"=>"1",
                        "waktu"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_kategori"=>$id_kategori
                    );

            $update = $this->mm->update_data("home_page_kategori" ,$data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
            }
            
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------page_kategori---------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------page_main-------------------------#
#=============================================================================#

    private function validate_input_page_menu(){
        $config_val_input = array(
                array(
                    'field'=>'nama_page',
                    'label'=>'Nama Halaman Menu',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'kategori',
                    'label'=>'Kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'next_page',
                    'label'=>'Link Selanjutnya',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_page_menu(){
        $detail_msg = array("nama_page"=>"", "kategori"=>"", "next_page"=>"", "foto_page"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_page_menu()){
            $nama_page = $this->input->post("nama_page");
            $kategori = $this->input->post("kategori");
            $next_page = $this->input->post("next_page");

            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->am->insert_page_main($kategori, $nama_page, $next_page, $time_update, $id_admin);
            $id_page = $insert->row_array()["id_page"];

            if($insert){
                $config['upload_path']          = './assets/core_img/icon_menu_layanan/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 200;
                $config['file_name']            = hash("sha256", $id_page).".jpg";
               
                $upload_data = $this->main_upload_file($config, "foto_page");
                // print_r($upload_data);
                if($upload_data["status"]){
                    $where = array("id_page"=>$id_page);
                    $set = array("foto_page"=>$upload_data["main_data"]["upload_data"]["file_name"]);
                    if($this->mm->update_data("home_page_main", $set, $where)){
                        $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else {
                    $detail_msg["foto_page"] = $this->response_message->get_error_msg("UPLOAD_FAIL");
                    $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }
        }else {
            $detail_msg["nama_page"] = strip_tags(form_error("nama_page"));
            $detail_msg["kategori"] = strip_tags(form_error("kategori"));
            $detail_msg["next_page"] = strip_tags(form_error("next_page"));
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);

        print_r(json_encode($msg_array));
    }


    public function get_page_menu(){
        $id = $this->encrypt->decode($this->input->post("id_page"));
        $data = $this->mm->get_data_each("home_page_main",array("id_page"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_page"=>$this->encrypt->encode($data["id_page"]),
                                                "nama_page"=>$data["nama_page"],
                                                "id_kategori"=>$data["id_kategori"],
                                                "next_page"=>$data["next_page"],
                                                "foto_page"=> base_url()."assets/core_img/icon_menu_layanan/".$data["foto_page"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_page_menu(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "nama_page"=>"",
                    "kategori"=>"",
                    "next_page"=>"",
                    "foto_page"=>""
                );

        if($this->validate_input_page_menu()){
            $id_page = $this->encrypt->decode($this->input->post("id_page"));

            $nama_page = $this->input->post("nama_page");
            $kategori = $this->input->post("kategori");
            $next_page = $this->input->post("next_page");

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto_page"])){
            #----cek empty or not----#
                if($_FILES["foto_page"]){
                    $config['upload_path']          = './assets/core_img/icon_menu_layanan/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 200;
                    $config['file_name']            = hash("sha256", $this->input->post("id_page")).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_page");
                    if($upload_data["status"]){
                        $set["foto_page"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_page"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }

                }
            }
            
            $set["nama_page"]   = $nama_page;
            $set["id_kategori"] = $kategori;
            $set["next_page"]   = $next_page;
            $set["waktu"]       = $time_update;
            $set["id_admin"]    = $id_admin;

            $where = array(
                "id_page"=>$id_page
            );

            if($this->mm->update_data("home_page_main", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["nama_page"] = strip_tags(form_error('nama_page'));
            $msg_detail["kategori"] = strip_tags(form_error('kategori'));
            $msg_detail["next_page"] = strip_tags(form_error('next_page'));

        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_page_menu(){
        $config_val_input = array(
                array(
                    'field'=>'id_page',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_page_menu(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        // print_r($_POST);
        if($this->val_form_page_menu()){
            $id_page = $this->encrypt->decode($this->input->post("id_page"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del,
                    "id_admin"=>$id_admin
                );

            $where = array("id_page"=>$id_page);

            if($this->mm->update_data("home_page_main", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=============================================================================#
#-------------------------------------------page_main-------------------------#
#=============================================================================#

}
?>