<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('admin_main', 'am');

        $this->load->library("response_message");
        $this->load->library("encrypt");
        // $this->encrypt->set_cipher(blowfish);
        
        $session = $this->session->userdata("admin_lv_1");

        // $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
        if(isset($session)){
            if($session["status_active"] == "1" and $session["is_log"] == "1"){
                if($this->encrypt->decode($session["id_lv"]) == "1"){
                    redirect(base_url("admin/super/home"));
                }
            }
        }
	}
    
    public function index(){
        $this->load->view('ad_super/admin_main/admin_login');
    }

    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    public function auth(){
        // $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
        
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("email" => "",
                            "password" => "");

        if($this->val_form_log()){
            $email = $this->input->post('email');
    		$password = $this->input->post('password');

    		$where = array(
    			'email' => $email,
    			'password' => hash("sha256", $password),
                'status_active' => "1",
                "a.is_del" => "0"
    			);
    		
            $cek = $this->am->select_admin($where);
    		if($cek){
                $data_session = array(
                                    "id_admin"  => $this->encrypt->encode($cek["id_admin"]),
                                    "id_lv"     => $this->encrypt->encode($cek["id_lv"]),
                                    "ket"       => $cek["ket"],
                                    "id_bidang" => $this->encrypt->encode($cek["id_bidang"]),
                                    "nama_dinas"=> $cek["nama_dinas"],
                                    "nama"      => $cek["nama"],
                                    "email"     => $cek["email"],
                                    "jabatan"   => $cek["jabatan"],
                                    "status_active" => $cek["status_active"],
                                    "is_log"    => "1"
                                );
                $this->session->set_userdata("admin_lv_1",$data_session);
                $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                
                if($this->encrypt->decode($data_session["id_lv"]) == 1){
                    redirect(base_url("admin/super/home"));
                }
    		}  
        }else{
            $msg_detail["email"]    = form_error("email");
            $msg_detail["password"] = form_error("password");

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,null);
        $this->session->set_flashdata("response_login", $msg_array);
        redirect(base_url("back-admin/login"));
    }
}
?>