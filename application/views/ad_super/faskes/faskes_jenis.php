<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Halaman Faskes - Jenis Faskes</h3>
        </div>
    </div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="card-title">Daftar Jenis Faskes Kota Malang</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                    <i class="fa fa-plus-circle"></i> Tambah
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="35%">Jenis Faskes</th>
                                        <th width="45%">Icon</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(isset($list_jenis)){
                                            if(!empty($list_jenis)){
                                                $no = 1;
                                                foreach ($list_jenis as $r_list_jenis => $v_list_jenis) {
                                                    print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_jenis->nama_jenis."</td>
                                                                <td>
                                                                    <center>
                                                                        <img id=\"img_list_jenis\" src=\"".base_url()."assets/core_img/icon_fk_jenis/".$v_list_jenis->icon_32."\" style=\"width: 32px; height: 32px;\">
                                                                        &nbsp;&nbsp;&nbsp;
                                                                        <img id=\"img_list_jenis\" src=\"".base_url()."assets/core_img/icon_fk_jenis/".$v_list_jenis->icon_64."\" style=\"width: 64px; height: 64px;\">
                                                                    </center>
                                                                </td>
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_jenis\" onclick=\"get_update_jenis('".$this->encrypt->encode($v_list_jenis->id_jenis)."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_jenis\" onclick=\"delete_jenis('".$this->encrypt->encode($v_list_jenis->id_jenis)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Tambah Jenis Faskes</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <div class="modal-body">                
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Keterangan Jenis Faskes</label>
                                <input type="text" name="ket_jenis" id="ket_jenis" class="form-control form-control-line">
                                <a id="msg_ket_jenis" style="color: red;"></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Icon 32 px</label>
                                <input type="file" name="icon_32" id="icon_32" class="form-control form-control-line">
                                <a id="msg_icon_32" style="color: red;"></a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Icon 64 px</label>
                                <input type="file" name="icon_64" id="icon_64" class="form-control form-control-line">
                                <a id="msg_icon_64" style="color: red;"></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <center><img id="img_icon_32" src="" style="width: 64px; height: 64px;"></center>
                        </div>
                        <div class="col-6">
                            <center><img id="img_icon_64" src="" style="width: 128px; height: 128px;"></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_insert_data" class="btn btn-info waves-effect">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Update Jenis Faskes</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">                
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Keterangan Jenis Faskes</label>
                                <input type="text" name="ket_jenis" id="_ket_jenis" class="form-control form-control-line">
                                <a id="_msg_ket_jenis" style="color: red;"></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Icon 32 px</label>
                                <input type="file" name="icon_32" id="_icon_32" class="form-control form-control-line">
                                <a id="_msg_icon_32" style="color: red;"></a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Icon 64 px</label>
                                <input type="file" name="icon_64" id="_icon_64" class="form-control form-control-line">
                                <a id="_msg_icon_64" style="color: red;"></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <center><img id="_img_icon_32" src="" style="width: 64px; height: 64px;"></center>
                        </div>
                        <div class="col-6">
                            <center><img id="_img_icon_64" src="" style="width: 128px; height: 128px;"></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_update_data" class="btn btn-info waves-effect">Ubah</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.mod8al-dialog -->
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================
    $(document).ready(function(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showLocation);
        }else{ 
            console.log('Geolocation is not supported by this browser.');
        }
    });

    function showLocation(position){
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log(position);
    }
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================

//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    var file_64 = [];
    $("#icon_64").change(function(e){
        file_64 = e.target.files[0];

        $("#img_icon_64").attr("src",URL.createObjectURL(file_64));
        // console.log(file);
    });  

    var file_32 = [];
    $("#icon_32").change(function(e){
        file_32 = e.target.files[0];

        $("#img_icon_32").attr("src",URL.createObjectURL(file_32));
        // console.log(file);
    });  

    $("#btn_insert_data").click(function(){
            var data_main =  new FormData();
            data_main.append('ket_jenis' , $("#ket_jenis").val());
            data_main.append('icon_32' , file_32);
            data_main.append('icon_64' , file_64);
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/add/faskes";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data jenis layanan kependudukan berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/fasilitas_jenis";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_ket_jenis").html(detail_msg.ket_jenis);
                        $("#msg_foto_jenis").html(detail_msg.foto_jenis);

                        swal("Proses Gagal.!!", "Data jenis layanan kependudukan gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
                
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_jenis_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_ket_jenis").val("");
        
        $("#_img_icon_32").attr("src", "");
        $("#_img_icon_64").attr("src", "");
        id_jenis_glob = "";
    }

    function get_update_jenis(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_jenis', param);
                                        
        $.ajax({
            url: "<?php echo base_url()."super/act/get/faskes";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                set_val_update(res, param);
                $("#update_data").modal('show');
            }
        });
    }

    function set_val_update(res, param){
        var res_pemohon = JSON.parse(res.toString());

        if(res_pemohon.status == true){
            var id_jenis_chahce = res_pemohon.val_response.id_jenis;
            $("#_ket_jenis").val(res_pemohon.val_response.nama_jenis);
            
            $("#_img_icon_32").attr("src", res_pemohon.val_response.icon_32);
            $("#_img_icon_64").attr("src", res_pemohon.val_response.icon_64);

            id_jenis_glob = id_jenis_chahce;
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    var file_64 = [];
    $("#_icon_64").change(function(e){
        file_64 = e.target.files[0];

        $("#_img_icon_64").attr("src",URL.createObjectURL(file_64));
        // console.log(file);
    });  

    var file_32 = [];
    $("#_icon_32").change(function(e){
        file_32 = e.target.files[0];

        $("#_img_icon_32").attr("src",URL.createObjectURL(file_32));
        // console.log(file);
    }); 

    $("#btn_update_data").click(function() {
        var data_main = new FormData();
        data_main.append('ket_jenis' , $("#_ket_jenis").val());
        
        data_main.append('icon_32' , file_32);
        data_main.append('icon_64' , file_64);

        data_main.append('id_jenis', id_jenis_glob);

        $.ajax({
            url: "<?php echo base_url()."super/act/up/faskes";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update(res);
            }
        });
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Proses Berhasil.!!",
                            text: "Data jenis layanan kependudukan berhasil disimpan ..!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#28a745",
                            confirmButtonText: "Lanjutkan",
                            closeOnConfirm: false
                        }, function() {
                            window.location.href = "<?php echo base_url()."admin/super/fasilitas_jenis";?>";
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        } else {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#_msg_ket_jenis").html(detail_msg.ket_jenis);
                        $("#_msg_icon_32").html(detail_msg.icon_32);
                        $("#_msg_icon_64").html(detail_msg.icon_64);

                        swal("Proses Gagal.!!", "Data jenis layanan kependudukan gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                    },

                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);

        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_jenis(param){
            !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus permanen, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: true 
                    }, function(){
                        
                        var data_main =  new FormData();
                        data_main.append('id_jenis', param);
                                                    
                        $.ajax({
                            url: "<?php echo base_url()."super/act/del/faskes";?>",
                            dataType: 'html',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,                         
                            type: 'post',
                            success: function(res){
                                console.log(res);
                                swal("Proses Berhasil.!!", "Penghapusan Data Berhasil", "success");
                                location.href="<?php print_r(base_url()."admin/super/fasilitas_jenis");?>";
                            }
                        });   
                    });                                     
                },
                                          
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>
