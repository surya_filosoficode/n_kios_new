<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Halaman Kesehatan Rumahsakit</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="card-title">Daftar Rumahsakit</h4>
                        </div>
                        <div class="col-lg-6 text-right">
                            <!-- <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#data_layanan">
                                <i class="fa fa-plus-circle"></i> Tambah Layanan
                            </button> -->
                            &nbsp;&nbsp;
                            <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_rs">
                                <i class="fa fa-plus-circle"></i> Tambah Rumah Sakit
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive m-t-100">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="25%">Nama Rumah Sakit</th>
                                    <th width="25%">Alamat</th>
                                    <th width="15%">No. Telepon</th>
                                    <th width="15%">Jenis Rumah Sakit</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($list_data_rs)){
                                        foreach ($list_data_rs as $r_list_data_rs => $v_list_data_rs) {
                                            echo "<tr>
                                                    <td>".($r_list_data_rs+1)."</td>
                                                    <td>".$v_list_data_rs->nama_rumah_sakit."</td>
                                                    <td>".$v_list_data_rs->alamat."</td>
                                                    <td>".$v_list_data_rs->telepon."</td>
                                                    <td>".$v_list_data_rs->nama_layanan."</td>
                                                    <td>
                                                        <button type=\"button\" class=\"btn btn-info\" id=\"up_rs\" onclick=\"func_update_rs('".$this->encrypt->encode($v_list_data_rs->id_rs)."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                        <button class=\"btn btn-danger\" id=\"del_rs\" onclick=\"delete_rs('".$this->encrypt->encode($v_list_data_rs->id_rs)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </td>
                                                </tr>";
                                        }
                                    }
                                ?>
                            </tbody>                  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ============================================================== -->
<!-- --------------------------insert_rs------------------------ -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  id="insert_rs" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Rumah Sakit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                                            
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_rumah_sakit" class="control-label">Nama Rumah Sakit :</label>
                                    <input type="text" class="form-control" id="nama_rumah_sakit" name="nama_rumah_sakit" required="">
                                    <p id="msg_nama_rumah_sakit" style="color: red;"></p>
                                </div>

                                <div class="form-group">
                                    <label for="alamat" class="control-label">Alamat :</label>
                                    <input type="text" class="form-control" id="alamat" name="alamat" required="">
                                    <p id="msg_alamat" style="color: red;"></p>
                                </div>

                                <div class="form-group">
                                    <label for="telepon" class="control-label">Telepon :</label>
                                    <input type="number" class="form-control" id="telepon" name="telepon" required="">
                                    <p id="msg_telepon" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="foto" class="control-label">Foto :</label>
                                    <input type="file" class="form-control" name="foto" id="foto" required="">
                                    <p id="msg_foto" style="color: red;"></p>
                                </div>

                                <center><img id="img_foto_rs" src="" style="width: 299px; height: 213px;"></center>
                            </div>
                            
                        </div>    
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="id_layanan" class="control-label">Jenis Layanan :</label>
                                    <select class="form-control" id="id_layanan" name="id_layanan" required="">

                                        <?php
                                            if($list_data_jenis){
                                                foreach ($list_data_jenis as $r_list_data_jenis => $v_list_data_jenis) {
                                                    print_r("<option value=\"".$v_list_data_jenis->id_layanan."\">".$v_list_data_jenis->nama_layanan."</option>");
                                                }
                                            }
                                        ?>
                                    </select>
                                    <p id="msg_id_layanan" style="color: red;"></p>
                                </div>
                            </div>                            
                        </div>    
                    </div>
                                         
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Ketersediaan Poli :</label>
                                </div>
                            </div>
                            
                                    <?php
                                        foreach ($list_data_poli as $r_list_data_poli => $v_list_data_poli) {
                                            print_r("<div class=\"col-md-4\">
                                                        <div class=\"form-group\">
                                                            <input type=\"checkbox\" class=\"b_check\" name=\"id_poli\" id=\"id_poli_".$v_list_data_poli->id_poli."\" value=\"".$v_list_data_poli->id_poli."\" data-checkbox=\"icheckbox_flat-blue\">&nbsp;&nbsp;
                                                            <label for=\"id_poli_".$v_list_data_poli->id_poli."\">".$v_list_data_poli->nama_poli."</label>
                                                    </div>
                                                        </div>");
                                        }
                                    ?>
                            <p id="msg_id_poli" style="color: red;"></p>&nbsp;                                
                        </div>    
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="add_rs" class="btn btn-primary">Simpan</button>
                </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------insert_rs------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_rs------------------------ -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  id="update_rs" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Rumah Sakit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_rumah_sakit" class="control-label">Nama Rumah Sakit :</label>
                                    <input type="text" class="form-control" id="_nama_rumah_sakit" name="nama_rumah_sakit" required="">
                                    <p id="msg_nama_rumah_sakit" style="color: red;"></p>
                                </div>

                                <div class="form-group">
                                    <label for="alamat" class="control-label">Alamat :</label>
                                    <input type="text" class="form-control" id="_alamat" name="alamat" required="">
                                    <p id="msg_alamat" style="color: red;"></p>
                                </div>

                                <div class="form-group">
                                    <label for="telepon" class="control-label">Telepon :</label>
                                    <input type="number" class="form-control" id="_telepon" name="telepon" required="">
                                    <p id="msg_telepon" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="foto" class="control-label">Foto :</label>
                                    <input type="file" class="form-control" name="foto" id="_foto" required="">
                                    <p id="msg_foto" style="color: red;"></p>
                                </div>

                                <center><img id="_img_foto_rs" src="" style="width: 299px; height: 213px;"></center>
                            </div>
                            
                        </div>    
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="id_layanan" class="control-label">Jenis Layanan :</label>
                                    <select class="form-control" id="_id_layanan" name="id_layanan" required="">

                                        <?php
                                            if($list_data_jenis){
                                                foreach ($list_data_jenis as $r_list_data_jenis => $v_list_data_jenis) {
                                                    print_r("<option value=\"".$v_list_data_jenis->id_layanan."\">".$v_list_data_jenis->nama_layanan."</option>");
                                                }
                                            }
                                        ?>
                                    </select>
                                    <p id="msg_id_layanan" style="color: red;"></p>
                                </div>
                            </div>                            
                        </div>    
                    </div>
                                         
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Ketersediaan Poli :</label>
                                </div>
                            </div>
                            
                                    <?php
                                        foreach ($list_data_poli as $r_list_data_poli => $v_list_data_poli) {
                                            print_r("<div class=\"col-md-4\">
                                                        <div class=\"form-group\">
                                                            <input type=\"checkbox\" class=\"a_check\" name=\"_id_poli\" id=\"_id_poli_".$v_list_data_poli->id_poli."\" value=\"".$v_list_data_poli->id_poli."\" data-checkbox=\"icheckbox_flat-blue\">&nbsp;&nbsp;
                                                            <label for=\"_id_poli_".$v_list_data_poli->id_poli."\">".$v_list_data_poli->nama_poli."</label>
                                                    </div>
                                                        </div>");
                                        }
                                    ?>
                            <p id="msg_id_poli" style="color: red;"></p>&nbsp;                                
                        </div>    
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="btn_update_rs" class="btn btn-primary">Ubah Data</button>
                </div>
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------update_rs------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    //========================================================================
    //--------------------------------Get_Lokasi------------------------------
    //========================================================================
        $(document).ready(function(){
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(showLocation);
            }else{ 
                console.log('Geolocation is not supported by this browser.');
            }
        });

        function showLocation(position){
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            // console.log(position);
        }
    //========================================================================
    //--------------------------------Get_Lokasi------------------------------
    //========================================================================

    var id_rs_glob = "";
    var key_ex, id_rs_op;
    
    //=========================================================================//
    //-----------------------------------insert_rs-----------------------------//
    //=========================================================================//

        var file = [];
        $("#foto").change(function(e){
            file = e.target.files[0];
            $("#img_foto_rs").attr("src",URL.createObjectURL(file));
            console.log(file);
        });

        $("#add_rs").click(function(){
            var choose = [];
            $.each($("input[name='id_poli']:checked"), function(){
                choose.push($(this).val());
            });

            var data_main =  new FormData();
            data_main.append('nama_rumah_sakit' , $("#nama_rumah_sakit").val());
            data_main.append('alamat', $("#alamat").val());
            data_main.append('telepon'  , $("#telepon").val());
            data_main.append('foto' , file);
            data_main.append('id_layanan', $("#id_layanan").val());
            data_main.append('id_poli'   , JSON.stringify(choose));
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/add/rs";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;

            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data Rumah sakit berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/rumah_sakit";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_nama_rumah_sakit").html(detail_msg.nama_rs);
                        $("#msg_alamat").html(detail_msg.alamat);
                        $("#msg_telepon").html(detail_msg.telepon);
                        $("#msg_id_layanan").html(detail_msg.id_layanan);
                        $("#msg_foto").html(detail_msg.foto);

                        swal("Proses Gagal.!!", "Data Rumah sakit gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
        }
    //=========================================================================//
    //-----------------------------------insert_rs-----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_rs_update-------------------------//
    //=========================================================================//
        function func_update_rs(id_rs){
            var id_rs_chahce = "";
            clear_from_update();

            var data_main =  new FormData();
            data_main.append('id_rs', id_rs);
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/get/rs";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    set_val_update(res, id_rs);
                    $("#update_rs").modal('show');
                }
            });
        }

        function set_val_update(res, id_rs){
            var res_pemohon = JSON.parse(res.toString());
            // console.log(res_pemohon.val_response.id_dinas);
            if(res_pemohon.status == true){
                id_rs_chahce = res_pemohon.val_response.id_rs;
                $("#_nama_rumah_sakit").val(res_pemohon.val_response.nama_rs);
                $("#_alamat").val(res_pemohon.val_response.alamat);
                $("#_telepon").val(res_pemohon.val_response.telepon);
                $("#_id_layanan").val(res_pemohon.val_response.id_layanan);

                $("#_img_foto_rs").attr("src", "<?php print_r(base_url());?>assets/core_img/icon_menu_rs/"+res_pemohon.val_response.foto);
                var poli = JSON.parse(res_pemohon.val_response.id_poli);

                $.each(poli, function(i, val){
                    $("#_id_poli_"+val).prop("checked", true);
                    console.log("#_id_poli_"+val);
                });
                console.log(poli);

                id_rs_glob = id_rs;
            }else {
                clear_from_update();
            }
        }

        function clear_from_update(){
            $("#_email").val("");
            $("#_nama").val("");
            $("#_nip").val("");
            $("#_jbtn").val("");
            $("#_lv").val("1");
            $("#_dinas").val("1");
            id_rs_glob = "";
            $("#update_rs").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_rs_update-------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_rs-----------------------------//
    //=========================================================================//

        var file = [];
        $("#_foto").change(function(e){
            file = e.target.files[0];
            $("#_img_foto_rs").attr("src",URL.createObjectURL(file));
            console.log(file);
        });

        $("#btn_update_rs").click(function(){
            var choose = [];
            $.each($("input[name='_id_poli']:checked"), function(){
                choose.push($(this).val());
            });

            var data_main =  new FormData();
            data_main.append('nama_rumah_sakit' , $("#_nama_rumah_sakit").val());
            data_main.append('alamat', $("#_alamat").val());
            data_main.append('telepon'  , $("#_telepon").val());
            data_main.append('foto' , file);
            data_main.append('id_layanan', $("#_id_layanan").val());
            data_main.append('id_poli'   , JSON.stringify(choose));
            data_main.append('id_rs' , id_rs_glob);
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/up/rs";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data rs berhasil diubah ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/rumah_sakit";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#_msg_nama_rumah_sakit").html(detail_msg.nama_rs);
                        $("#_msg_alamat").html(detail_msg.alamat);
                        $("#_msg_telepon").html(detail_msg.telepon);
                        $("#_msg_id_layanan").html(detail_msg.id_layanan);
                        $("#_msg_foto").html(detail_msg.foto);
                        
                        swal("Proses Gagal.!!", "Data rs gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_rs-----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------rs_delete-----------------------------//
    //=========================================================================//
        function delete_rs(id_rs){
            // console.log(id_rs);
            !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: false 
                    }, function(){
                        var data_main =  new FormData();
                        data_main.append('id_rs', id_rs);
                                                    
                        $.ajax({
                            url: "<?php echo base_url()."super/act/del/rs";?>",
                            dataType: 'html',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,                         
                            type: 'post',
                            success: function(res){
                                // console.log(res);
                                response_delete(res);
                            }
                        });
                    });                              
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),
            
            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if(main_msg.status){
                // console.log("true");
                
                swal({   
                    title: "Proses Berhasil.!!",   
                    text: "Data rs berhasil dihapus ..!",   
                    type: "success",   
                    showCancelButton: false,   
                    confirmButtonColor: "#28a745",   
                    confirmButtonText: "Lanjutkan",   
                    closeOnConfirm: false 
                }, function(){
                    window.location.href = "<?php echo base_url()."admin/super/rumah_sakit";?>";
                });                              
                    

            }else{
                // console.log("false");
                swal("Proses Gagal.!!", "Data rs gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");                          
            }
        }
    //=========================================================================//
    //-----------------------------------rs_update-----------------------------//
    //=========================================================================//
        
</script>