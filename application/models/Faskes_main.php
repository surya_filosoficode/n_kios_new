<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Faskes_main extends CI_Model{

#=================================================================================================#
#-------------------------------------------jenis-------------------------------------------------#
#=================================================================================================#  
    public function insert_jenis($nama_jenis, $time_update, $id_admin){
        return $this->db->query("select insert_faskes_jenis('".$nama_jenis."',  '".$time_update."', '".$id_admin."') as id_jenis;")->row_array();
    }
#=================================================================================================#
#-------------------------------------------jenis-------------------------------------------------#
#=================================================================================================#  
  



#=================================================================================================#
#-------------------------------------------faskes_main-------------------------------------------#
#=================================================================================================#
    public function insert_faskes($id_jenis, $nama_faskes, $foto_faskes, $lokasi, $detail_faskes, $time_update, $id_admin){
        return $this->db->query("SELECT insert_faskes_main('".$id_jenis."', '".$nama_faskes."', '".$foto_faskes."', \"".$lokasi."\", \"".$detail_faskes."\", '".$time_update."', '".$id_admin."') AS id_faskes;")->row_array();
    }

    public function get_faskes_all($where){
        $this->db->join("faskes_jenis pj"   , "pm.id_jenis = pj.id_jenis");
        return $this->db->get_where("faskes_main pm", $where)->result();          
    }

    public function get_detail_faskes_api($where){
    	$this->db->select("mk.nama_kecamatan, pj.nama_jenis, sha2(id_faskes, 512) as id_faskes, foto_sklh, lokasi, nama_faskes, detail_faskes");
        $this->db->join("master_kecamatan mk", "ps.id_kecamatan = mk.id_kecamatan");
        $this->db->join("pendidikan_jenis pj", "ps.id_jenis = pj.id_jenis");
        return $this->db->get_where("pendidikan_faskes ps", $where)->result();    	
    }
#=================================================================================================#
#-------------------------------------------faskes_main-------------------------------------------#
#=================================================================================================#


}
?>